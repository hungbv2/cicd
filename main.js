const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
	res.status(200).json({
		message : 'CICD HELLO WORLD',
		status: 200
	})
})

app.listen('3000', () => {
	console.log(`Listening on port 3000`)
})
